export class News {
  constructor(
    public id: string,
    public title: string,
    public description: string,
    public date: string,
    public image: string,
  ) {}
}

export class Comments {
  constructor(
    public id: string,
    public author: string,
    public comments: string,
    public news_id: string
  ) {
  }
}

export interface NewsData {
  [key: string]: any;
  title: string;
  description: string;
  date: string;
  image: File | null;
}

export interface CommentData{
  [key: string]: any;
  author: string;
  comments: string;
  news_id: string;
}
