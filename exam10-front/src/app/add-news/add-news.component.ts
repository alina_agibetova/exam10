import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NewsService } from '../services/news.service';
import { Router } from '@angular/router';
import { NewsData } from '../models/news.model';

@Component({
  selector: 'app-add-news',
  templateUrl: './add-news.component.html',
  styleUrls: ['./add-news.component.sass']
})
export class AddNewsComponent implements OnInit {
  @ViewChild('f') form!: NgForm;

  constructor(private newsService: NewsService, private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit() {
    const value: NewsData = this.form.value;
    this.newsService.createNews(value).subscribe(() => {
      void this.router.navigate(['/'])
    });
  }
}
