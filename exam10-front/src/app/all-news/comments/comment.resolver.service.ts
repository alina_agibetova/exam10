import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { NewsService } from '../../services/news.service';
import { Injectable } from '@angular/core';
import { Comments, News } from '../../models/news.model';


@Injectable({
  providedIn: 'root'
})
export class CommentResolverService implements Resolve<Comments>{

  constructor(private newsService: NewsService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Comments> | Observable<never>{
    const newsId = <string>route.params['id'];
    return this.newsService.getComment(newsId).pipe(mergeMap(news => {
      if (news){
        return of (news);
      }
      void this.router.navigate(['/']);
      return EMPTY;
    }));
  }
}
