import { Component, OnInit } from '@angular/core';
import { NewsService } from '../../services/news.service';
import { ActivatedRoute, Data } from '@angular/router';
import { Comments, News } from '../../models/news.model';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.sass']
})
export class CommentsComponent implements OnInit {
  news!: News;
  constructor(private newsService: NewsService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.data.subscribe((data: Data) => {
      this.news = <News>data['id'];
      console.log(<News>data['id']);
    });
  }

  onRemove() {
    this.newsService.removeComment(this.news.id).subscribe(()=> {
      this.newsService.getNews();
    });
  }
}
