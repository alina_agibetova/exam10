import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllNewsComponent } from './all-news/all-news.component';
import { AddNewsComponent } from './add-news/add-news.component';
import { CommentsComponent } from './all-news/comments/comments.component';
import { CommentResolverService } from './all-news/comments/comment.resolver.service';

const routes: Routes = [
  {path: '', component: AllNewsComponent},
  {path: 'all/add', component: AddNewsComponent},
  {path: ':id', component: CommentsComponent,
    resolve: {
      dish: CommentResolverService
    }},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
