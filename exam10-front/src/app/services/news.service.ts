import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Comments, News, NewsData } from '../models/news.model';
import { map } from 'rxjs';
import { Comment } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HttpClient) { }

  getNews(){
    return this.http.get<News[]>('http://localhost:8000/items').pipe(
      map(response => {
        return response.map(newsData => {
          return new News(
            newsData.id,
            newsData.title,
            newsData.description,
            newsData.date,
            newsData.image,
          );
        });
      })
    )
  }

  getComment(id: string){
    return this.http.get<Comments | null >(`http://localhost:8000/comments/${id}`).pipe(
      map(result => {
        if (!result) {
          return null;
        }
        return new Comments(result.id, result.author, result.comments, result.news_id);
      })
    );
  }

  createNews(newsData: NewsData){
    const formData = new FormData();
    formData.append('title', newsData.title);
    formData.append('description', newsData.description);
    formData.append('date', newsData.date);

    if (newsData.image){
      formData.append('image', newsData.image);
    }

    return this.http.post('http://localhost:8000/items', formData);
  }

  removeComment(id:string){
    return this.http.delete(`http://localhost:8000/comments/${id}`)
  }
}
