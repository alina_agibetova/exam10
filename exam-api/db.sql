create table items
(
    id          int auto_increment
        primary key,
    title       varchar(255) not null,
    description text         not null,
    image       int          null,
    date        timestamp    null
);

create table comments
(
    id       int auto_increment
        primary key,
    author   varchar(255) null,
    comments text         null,
    news_id  int          null,
    constraint comments_items_id_fk
        foreign key (news_id) references items (id)
            on delete cascade
);

insert into all-news.comments (id, author, comments, news_id)
values  (2, 'Alina', 'some', 1),
        (3, 'Ermek', 'ssss', 1);

insert into all-news.items (id, title, description, image, date)
values  (1, 'aaaa', 'gfgfgf', null, '2022-02-12 16:14:37'),
        (2, 'about', 'ermek', null, '2022-02-12 18:19:53');