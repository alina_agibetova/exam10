const express = require('express');
const db = require('../mySqlDb');

const router = express.Router();

router.get('/', async (req, res,next) => {
  try {
    let query = 'SELECT * FROM comments';

    if (req.query.orderBy === 'id' && req.query.direction === 'desc') {
      query += ' ORDER BY id DESC';
    }

    let [comments] = await db.getConnection().execute(query);
    return res.send(comments);
  } catch (e) {
    next(e);
  }

});

router.get('/:id', async (req, res, next) => {
  try {
    const [comments] = await db.getConnection().execute('SELECT * FROM comments WHERE id = ?', [req.params.id]);
    const comment = comments[0];
    if (!comment){
      return res.status(404).send({message: 'Not found'});
    }
    return res.send(comment);
  } catch (e) {
    next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    if (!req.body.author|| !req.body.comments){
      return res.status(400).send({message: 'Title and description are required'})
    }

    const comments = {
      author: req.body.author,
      comments: req.body.comments,
      news_id: req.body.news_id,
    };

    let query = 'INSERT INTO comments (title, comments, news_id) VALUES (?, ?, ?)';

    const results = await db.getConnection().execute(query, [
      comments.author,
      comments.comments,
      comments.news_id
    ]);

    const newComments = {
      id: results.insertId,
      author: req.body.author,
      comments: req.body.comments,
      news_id: req.body.news_id,
    };

    return res.send(newComments);
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    const [results] = await db.getConnection().execute('DELETE FROM comments WHERE id = ?', [req.params.id]);
    return res.send(results)
  } catch (e) {
    next(e);
  }
})

module.exports = router;
