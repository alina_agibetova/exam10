const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const db = require('../mySqlDb');

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },

  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res,next) => {
  try {
    let query = 'SELECT * FROM items';

    if (req.query.filter === 'image'){
      query += 'WHERE image IS NOT NULL';
    }

    if (req.query.orderBy === 'datetime' && req.query.direction === 'desc') {
      query += ' ORDER BY id DESC';
    }

    let [items] = await db.getConnection().execute(query);
    return res.send(items);
  } catch (e) {
    next(e);
  }

});

router.get('/:id', async (req, res, next) => {
  try {
    const [items] = await db.getConnection().execute('SELECT * FROM items WHERE id = ?', [req.params.id]);
    const item = items[0];
    if (!item){
      return res.status(404).send({message: 'Not found'});
    }
    return res.send(item);
  } catch (e) {
    next(e);
  }
});

router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.title){
      return res.status(400).send({message: 'Title is required'})
    }

    const items = {
      title: req.body.title,
      description: req.body.description,
      image: null,
      date: req.body.date,
    };

    if (req.file) {
      items.image = req.file.filename;
    }

    let query = 'INSERT INTO items (title, description, image, date) VALUES (?, ?, ?, ?)';

    const results = await db.getConnection().execute(query, [
      items.title,
      items.description,
      items.image,
      items.date,
    ]);

    const newNews = {
      id: results.insertId,
      title: req.body.title,
      description: req.body.description,
      image: null,
      date: req.body.date,
    };

    return res.send(newNews);
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    const [results] = await db.getConnection().execute('DELETE FROM items WHERE id = ?', [req.params.id]);
    return res.send(results)
  } catch (e) {
    next(e);
  }
})

module.exports = router;
